require File.expand_path('../../unit_test_helper', __FILE__)

class MockKiller < JobKiller
  attr_accessor :queue
end

describe "EventNotificationWorker" do
  attr_accessor :event, :job, :killer
  before do
    @event = MiniTest::Mock.new
    @job = MiniTest::Mock.new
    @killer = MockKiller.new
    @killer.queue = [@job]
  end

  it "should not cause any jobs to be killed if the job ids don't match" do
    job.expect(:jid, 'abracadabra')
    event.expect(:notification_job_id, 'alakazam')
    event.expect(:notification_job_id=, 'neato', ['neato'])
    event.expect(:save, true)

    setup_context do
      EventNotificationWorker.perform_async(:event_id => 1234)
    end
    event.verify
    job.verify
  end

  it "should cause a job to be killed if the job ids matched" do
    job.expect(:jid, 'abracadabra')
    job.expect(:delete, true)
    event.expect(:notification_job_id, 'abracadabra')
    event.expect(:notification_job_id=, 'neato', ['neato'])
    event.expect(:save, true)

    setup_context do
      EventNotificationWorker.perform_async(:event_id => 1234)
    end
    event.verify
    job.verify
  end

  it "should not even check a jobs id if the event doesn't have a job id" do
    event.expect(:notification_job_id, nil)
    event.expect(:notification_job_id=, 'neato', ['neato'])
    event.expect(:save, true)

    setup_context do
      EventNotificationWorker.perform_async(:event_id => 1234)
    end
    event.verify
    job.verify
  end

  it "should not try to save an event if there was an issue enqueing the job" do
    event.expect(:notification_job_id, nil)
    setup_context(:job_id => false) do
      EventNotificationWorker.perform_async(:event_id => 1234)
    end
    event.verify
  end

  private
  # mmm that tasty tasty context :(
  def setup_context(params={:job_id => 'neato'})
    Persistence::Event.stub :get, (params[:event] || event) do
      EventNotificationWorker.stub :job_killer, (params[:killer] || killer) do
        EventNotificationWorker.stub :enqueue, params[:job_id] do
          yield
        end
      end
    end
  end

end