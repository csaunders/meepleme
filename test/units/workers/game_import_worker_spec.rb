require File.expand_path('../../unit_test_helper', __FILE__)

class MockBGGApi
  def collection(*args)
    {"totalitems" => 0, "termsofuse" => "dunnolol", "item" => []}
  end
end

describe "GameImportWorker" do
  before do
    @worker = GameImportWorker.new
    @worker.api_client = MockBGGApi.new
  end

  it "should be possible to pass in a mocked API client" do
    @worker.perform(:bgg_name => 'jiblits', :api_client_name => MockBGGApi.to_s)
    assert @worker.api_client.is_a?(MockBGGApi)
  end

  it "should save all of the items" do
    @worker.api_client.stub :collection, mock_api_results do
      @worker.stub :save, true do
        @worker.perform(:bgg_name => 'jiblits', :api_client_name => MockBGGApi.to_s)
      end
    end
  end

  it "it should properly extract api results into boardgames" do
    extracted_collection = @worker.extract_collection(mock_api_results)
    assert_equal 2, extracted_collection.length
    wonders = extracted_collection.first
    wonders_cities = extracted_collection.last

    assert_equal "7 Wonders", wonders.name
    assert_equal 68448, wonders.bgg_id
    assert_match "pic860217.jpg", wonders.cover_art_url.path

    assert_equal "7 Wonders: Cities", wonders_cities.name
    assert_equal 111661, wonders_cities.bgg_id
    assert_match "pic1380423.jpg", wonders_cities.cover_art_url.path
  end

  it "should be able to extract game name information from an entry" do
    assert_equal '7 Wonders', @worker.extract_name(game)
  end

  it "should be able to extract image url information from an entry" do
    expected = 'http://cf.geekdo-images.com/images/pic860217.jpg'
    assert_equal expected, @worker.extract_image_url(game)
  end

  it "should be able to extract the bgg id as an integer" do
    assert_equal 68448, @worker.extract_bgg_id(game)
  end

  it "it should fire off the worker for associating the games with a user" do
    mock_worker = MiniTest::Mock.new
    mock_worker.expect(:perform_async, true, [{:user_id => 123, :boardgame_ids => [1]}])

    boardgame = MiniTest::Mock.new
    boardgame.expect(:id, 1)

    mock_user = MiniTest::Mock.new
    mock_user.expect(:id, 123)
    mock_user_model = MiniTest::Mock.new
    mock_user_model.expect(:first, mock_user, [{:bggname => 'fancy'}])
    @worker.bgg_name = 'fancy'

    @worker.stub(:associate_user_with_collection_worker, mock_worker) do
      @worker.stub(:user_model, mock_user_model) do
        @worker.associate([boardgame])
      end
    end
    mock_worker.verify
  end

  it "should try to find a boardgame before trying to create a new instance of one" do
    mock_object = MiniTest::Mock.new
    mock_object.expect(:valid?, true)
    mock_model = MiniTest::Mock.new
    params = {'name' => [{'content' => "neat"}], 'image' => ["http://example.com"], 'objectid' => '123'}
    mock_object.expect(:name=, nil, ['neat'])
    mock_object.expect(:cover_art_url=, nil, ["http://example.com"]) 
    mock_model.expect(:first_or_new, mock_object, [{:bgg_id => 123}])

    @worker.stub :boardgame_model, mock_model do
      @worker.construct_boardgame(params)
      assert mock_object.valid?
    end
    mock_model.verify
    mock_object.verify
  end

  def game
    mock_api_results['item'].first
  end

  def mock_api_results
    {
      "totalitems"=>"2", 
      "termsofuse"=>"http://boardgamegeek.com/xmlapi/termsofuse",
      "item"=>[
        {
          "objecttype"=>"thing",
          "objectid"=>"68448",
          "subtype"=>"boardgame",
          "collid"=>"15068923",
          "name"=>[
            {"sortindex"=>"1", "content"=>"7 Wonders"}
          ],
          "yearpublished"=>["2010"],
          "image"=>["http://cf.geekdo-images.com/images/pic860217.jpg"],
          "thumbnail"=>["http://cf.geekdo-images.com/images/pic860217_t.jpg"],
          "status"=>[
            {"own"=>"1", "prevowned"=>"0", "fortrade"=>"0", "want"=>"0", "wanttoplay"=>"0", "wanttobuy"=>"0", "wishlist"=>"0", "preordered"=>"0", "lastmodified"=>"2012-03-29 18:58:20"}
          ],
          "numplays"=>["2"]
        },
        {
          "objecttype"=>"thing",
          "objectid"=>"111661",
          "subtype"=>"boardgame",
          "collid"=>"17009282",
          "name"=>[
            {"sortindex"=>"1", "content"=>"7 Wonders: Cities"}
          ],
          "yearpublished"=>["2012"],
          "image"=>["http://cf.geekdo-images.com/images/pic1380423.jpg"],
          "thumbnail"=>["http://cf.geekdo-images.com/images/pic1380423_t.jpg"],
          "status"=>[
            {"own"=>"1", "prevowned"=>"0", "fortrade"=>"0", "want"=>"0", "wanttoplay"=>"0", "wanttobuy"=>"0", "wishlist"=>"0", "preordered"=>"0", "lastmodified"=>"2013-02-08 10:15:14"}
          ],
          "numplays"=>["0"]
        }
      ]
    }
  end

end