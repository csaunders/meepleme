require File.expand_path('../../unit_test_helper', __FILE__)

describe "UserCollectionWorker" do
  attr_accessor :mock_user, :mock_search_results
  before do
    @worker = UserCollectionWorker.new
    @mock_user = MiniTest::Mock.new
  end

  it "it should attach all the boardgames to the user" do
    user_id = 123
    boardgame_ids = [1,2,3,4,5]

    mock_user_model = MiniTest::Mock.new
    mock_user_model.expect(:get, mock_user, [user_id])
    mock_boardgame_model = MiniTest::Mock.new
    mock_boardgame_model.expect(:all, mock_search_results, [{:id => boardgame_ids}])

    mock_user.expect(:boardgames, ['fancy'])
    mock_user.expect(:boardgames=, [], [['fancy'].concat(mock_search_results)])
    mock_user.expect(:save, true)

    @worker.stub(:user_model, mock_user_model) do
      @worker.stub(:boardgame_model, mock_boardgame_model) do
        assert @worker.perform(:user_id => user_id, :boardgame_ids => boardgame_ids)
      end
    end
    mock_user.verify
  end

  def mock_search_results
    @results ||= [MiniTest::Mock.new]
  end

end