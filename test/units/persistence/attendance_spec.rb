require File.expand_path('../../unit_test_helper', __FILE__)

describe "Attendance" do
  attr_accessor :event, :user, :attendance
  before do
    @event = Persistence::Event.new
    @user  = Persistence::User.new
    @attendance = Persistence::Attendance.new
  end

  it "should initialize an attendance to invited" do
    attendance = Persistence::Attendance.new(:event => event, :user => user)
    assert_equal 'invited', attendance.status
  end

  it "should be possible for an invited attendance to become accepted" do
    attendance.accept!
    assert_equal 'accepted', attendance.status
  end

  it "should be possible for an invited attendance to become declined" do
    attendance.decline!
    assert_equal 'declined', attendance.status
  end

  it "should be possible for an accepted attendance to decline" do
    attendance.accept!
    attendance.decline!
    assert_equal 'declined', attendance.status
  end

  it "should be possible for a declined attendance to accept" do
    attendance.decline!
    attendance.accept!
    assert_equal 'accepted', attendance.status
  end

end