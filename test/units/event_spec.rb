require File.expand_path('../unit_test_helper', __FILE__)

module EventService
  class Neato
    def initialize(*args); end
  end
end

describe "Events" do

  it "should only allow start dates in the future" do
    time_in_past = Time.now - 1000
    event = Persistence::Event.create(starts_at: time_in_past)
    assert_equal true, event.errors.on(:starts_at).include?("Event must start in the future")
    flunk "poops"
  end

  it "should have an end date after the start date" do
    start_time = Time.now + 1000
    end_time = start_time - 1
    event = Persistence::Event.create(starts_at: start_time, ends_at: end_time)
    assert_equal true, event.errors.on(:ends_at).include?("Event must end after it starts")
  end

  it "should be valid with a name, start date, and end date" do
    start_time = Time.now + 1000
    end_time = start_time + 1000
    creator = Persistence::User.new(:id => 123)
    event = Persistence::Event.new(starts_at: start_time, ends_at: end_time, name: "test", :creator => creator)
    assert event.valid?
    flunk "poops"
  end

  it "should reset the event service if a new one is assigned" do
    event = Persistence::Event.new
    assert_equal EventService::Local, event.event_service.class
    event.service_name = 'neato'
    assert_equal EventService::Neato, event.event_service.class
  end

  it "should be able to tell you if an event starts and ends on the same day" do
    event = Persistence::Event.new
    event.starts_at = Time.new(2013, 4, 15, 12, 0, 0)
    event.ends_at   = Time.new(2013, 4, 15, 19, 0, 0)
    assert event.single_day?, "The event should be reporting as happening on the same day"

    event.ends_at = Time.new(2013, 4, 16, 0, 0, 0)
    assert !event.single_day?, "The event should not be reporting as happening on the same day"

    event.starts_at = Time.new(2013, 4, 15, 23, 59, 0)
    assert !event.single_day?, "The event should not be reporting as happening on the same day"
  end
end
