require File.expand_path('../../unit_test_helper', __FILE__)

class MockUser
  def bggname
  end
end

describe "UserFlowRouter" do
  attr_accessor :user, :user_router

  before do
    @user = MockUser.new
    @user_router = UserFlowRouter.new(user)
  end

  it "should return nil if there is no logged in user" do
    user_router.user = nil
    assert_equal nil, user_router.route
  end

  it "should be possible dynamically change how routes resolve" do
    routes = [
      Routable.new(:validator => lambda {|user| user.is_a_panda?}, :route => "/no-pandas")
    ]
    user = MiniTest::Mock.new
    user.expect :is_a_panda?, true
    user_router.routes = routes
    user_router.user = user
    
    assert_equal '/no-pandas', @user_router.route
  end

  it "should redirect the user to their account page for more details if the just signed up" do
    user.stub :bggname, nil do
      assert_equal '/', user_router.route
    end
  end

  it "should redirect the user to the events page if they have entered their BGG account" do
    user.stub :bggname, 'jiblits' do
      assert_equal '/events', user_router.route
    end
  end

  it "should raise an exception if the user is in an invalid state" do
      user_router.routes = {}
      user_router.user = MiniTest::Mock.new
      user_router.user.expect :id, 123

      assert_raises UserFlowRouter::InvalidRoutingError do
        user_router.route
      end
  end
end