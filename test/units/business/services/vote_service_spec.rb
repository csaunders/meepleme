require File.expand_path('../../../unit_test_helper', __FILE__)

describe "VoteService" do
  attr_accessor :event, :boardgame, :user, :attendance

  before do
    @event = MiniTest::Mock.new
    @boardgame = Persistence::Boardgame.new
    @user = MiniTest::Mock.new
    @attendance = Persistence::Attendance.new
  end

  it "should initialize a new vote when the #vote is called" do
    attendances = MiniTest::Mock.new
    attendances.expect(:first, attendance, [:event => event])
    @user.expect(:attendances, attendances)

    service = VoteService.new(:event => event, :boardgame => boardgame, :user => user)

    vote = service.vote
    assert_equal attendance, vote.attendance
    assert_equal boardgame, vote.boardgame
  end

  it "should create both a vote and save it when #vote! is called" do
    attendances = MiniTest::Mock.new
    attendances.expect(:first, attendance, [:event => event])
    @user.expect(:attendances, attendances)

    service = VoteService.new(:event => event, :boardgame => boardgame, :user => user)
    vote = MiniTest::Mock.new
    service.stub :vote, vote do
      vote.expect(:save, true)
      assert service.vote!
      vote.verify
    end
  end
end