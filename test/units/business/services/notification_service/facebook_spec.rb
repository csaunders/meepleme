require File.expand_path('../../../../unit_test_helper', __FILE__)

# reopening the class so we can expose variables
module NotificationService
  class Facebook
    attr_accessor :games
  end
end

describe "NotificationService::Base" do
  Gamey = Struct.new(:name, :votes)
  Eventy = Struct.new(:service_id)

  attr_accessor :event, :notifier
  before do
    @event = Eventy.new
    @notifier = NotificationService::Facebook.new(@event)
  end

  it "should be able to render the template" do
    notifier.games = [
      Gamey.new('7 Wonders', 10)
    ]
    output = notifier.render
    assert_match /The games that we will be playing with are:$/, output
    assert_match /7 Wonders with 10 votes$/, output
  end

  it "should be able to render the template with multiple games" do
    notifier.games = [
      Gamey.new('7 Wonders', 10),
      Gamey.new('Settlers of Catan', 3)
    ]
    output = notifier.render
    assert_match /7 Wonders with 10 votes$/, output
    assert_match /Settlers of Catan with 3 votes$/, output
  end

  it "should be able to votes in singular" do
    notifier.games = [
      Gamey.new('7 Wonders', 1)
    ]
    output = notifier.render
    assert_match /7 Wonders with 1 vote$/, output
  end

  it "should render a game with no votes in plural" do
    notifier.games = [
      Gamey.new('7 Wonders', 0)
    ]
    output = notifier.render
    assert_match /7 Wonders with 0 votes$/, output
  end

  it "notifying should send a request off to facebook" do
    fake_graph = MiniTest::Mock.new
    fake_graph.expect(:put_connections, nil, [1234, 'feed', {message: 'snicker doodle'}])
    event.service_id = 1234
    @notifier.stub :notification_message, 'snicker doodle' do
      @notifier.stub :graph, fake_graph do
        notifier.notify
      end
    end
    fake_graph.verify
  end

end