require File.expand_path('../../../../unit_test_helper', __FILE__)

module NotificationService
  class SomeNotifier < Base
    def notify; end
  end
end

describe "NotificationService::Base" do
  attr_accessor :event
  before do
    @event = MiniTest::Mock.new
  end

  it "should be able to initialize a notifier given a string" do
    notifier = NotificationService::Base.notifier(event, "some_notifier")
    assert_equal NotificationService::SomeNotifier, notifier.class
  end

  it "should return the NilNotifier when nothing is passed in" do
    notifier = NotificationService::Base.notifier(event)
    assert_equal NotificationService::NilNotifier, notifier.class
  end

  it "should not be possible to try and send notifications from the base service" do
    notifier = NotificationService::Base.new(event)
    assert_raises NotImplementedError do
      notifier.notify
    end
  end
end