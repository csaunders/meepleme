require File.expand_path('../../../../unit_test_helper', __FILE__)

class NilGraph
  def method_missing(method, args, *block); end
end

class FauxUser < MiniTest::Mock
  def initialize(*args)
    super()
    expect(:save, true)
    expect(:class, FauxUser)
  end

  def self.first(*args); end
end

describe "EventService::Facebook" do
  attr_accessor :event, :creator
  before do
    @event = MiniTest::Mock.new
    @creator = MiniTest::Mock.new
    creator.expect(:id, 1)
  end

  it "should provide a hash of parameters that should be stored for background processing" do
    event.expect(:id, 1000)
    expected_job_params = {
      :event_id => 1000,
      :invitations => [1,2,3]
    }
    facebook = EventService::Facebook.new(event)
    facebook.invitations = [1,2,3]
    assert_equal expected_job_params, facebook.default_params
    event.verify
  end

  it "should create invitations for users who already exist in the database" do
    attendances = MiniTest::Mock.new
    user = MiniTest::Mock.new
    user.expect(:save, true)

    attendance = MiniTest::Mock.new
    attendance.expect(:save, true)

    attendances.expect(:new, attendance, [:user => user])

    event.expect(:attendances, attendances)
    event.expect(:service_id, 1234)

    Persistence::User.stub :first, user do
      facebook = EventService::Facebook.new(event)
      facebook.invitations = [1000]
      facebook.stub :graph, NilGraph.new do
        facebook.invite
      end
    end
    event.verify
    attendances.verify
    attendance.verify
    user.verify
  end

  it "should create ghost users for people who have been invited but haven't joined the app yet" do
    attendances = MiniTest::Mock.new
    attendance = MiniTest::Mock.new
    attendance.expect(:save, true)

    attendances.expect(:new, attendance) do |args|
      args.length == 1 && args.first[:user].class == FauxUser
    end
    event.expect(:attendances, attendances)
    event.expect(:service_id, 1234)
    facebook = EventService::Facebook.new(event)
    facebook.invitations = [1000]
    facebook.stub :graph, NilGraph.new do
      facebook.stub :user_class, FauxUser do
        facebook.invite
      end
    end
  end

  def starts_at
    DateTime.strptime('2012-03-05 12:00', "%Y-%m-%d %H:%M")
  end

  def ends_at
    DateTime.strptime('2012-03-06 12:00', "%Y-%m-%d %H:%M")
  end

end