require File.expand_path('../../unit_test_helper', __FILE__)

describe "Routable" do
  attr_accessor :routable
  before do
    self.routable = Routable.new :route => '/'
  end

  it "should be possible to pass in a lambda as the validator" do
    thinger = MiniTest::Mock.new
    thinger.expect :fancy?, true
    routable.validator = lambda {|thinger| thinger.fancy?}
    assert routable.resolves?(thinger)
  end

  it "should be possible to create a subclass with it's own custom validator" do
    class CustomRoutable < Routable
      def resolves?(item)
        item == 'thinger'
      end
    end
    routable = CustomRoutable.new
    assert !routable.resolves?('thanger')
    assert routable.resolves?('thinger')
  end

  it "should be possible set the route" do
    routable.route = '/thingy'
    assert_equal '/thingy', routable.route
  end

end