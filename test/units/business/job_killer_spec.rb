require File.expand_path('../../unit_test_helper', __FILE__)

describe "JobKiller" do

  it "should call kill on a job if the test returns true" do
    killer = JobKiller.new
    job = MiniTest::Mock.new
    job.expect(:delete, true)
    killer.stub :queue, [job] do
      killer.kill_job do |job|
        true
      end
    end
    job.verify
  end

  it "should not call kill on a job if the test returns false" do
    killer = JobKiller.new
    job = MiniTest::Mock.new
    killer.stub :queue, [job] do
      killer.kill_job do |job|
        false
      end
    end
  end

end