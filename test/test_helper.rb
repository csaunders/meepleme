require "rubygems"
require "bundler/setup"

ENV['RACK_ENV'] = 'test'
require 'minitest/spec'
require 'minitest/mock'
require 'minitest/autorun'
require 'minitest/pride'
require 'minitest/reporters'

begin
  require_relative('../app/application')
rescue NameError
  require File.expand_path('../../app/application', __FILE__)
end

MiniTest::Reporters.use! [MiniTest::Reporters::ProgressReporter.new]