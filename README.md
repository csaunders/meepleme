# Meeple.Me

Meepling the meeples so you can meeple while you meeple with meeples
up in the meeple of meeplely meepleness.

### Da Rulez

Let's try to abide by these 4 simple rules for our coding standards. They
should force us to think hard enough about our design decisions and might
even make us strongly question some code when we see it.

These aren't written in stone, but if you want to break da rulez you
better have a good reason for it :)

Here are da rulez:

1. Classes can be no longer than one hundred lines of code.
2. Methods can be no longer than five lines of code.
3. Pass no more than four parameters into a method. Hash options are parameters.
4. Controllers can instantiate only one object. Therefore, views can only know about one instance variable and views should only send messages to that object (@object.collaborator.value is not allowed).

### Getting Started

 1. Clone the repo
 2. run `bundle install` to install dependancies
 3. run `compass compile` to generate css
 4. Drink something fancy (adding an umbrella to anything makes it fancy)
 5. Profit

### Running Server

The basic way of getting the server going is as follows:

    APP_ID=<get this from your friendly neighbourhood developer> APP_SECRET=<see previous> bundle exec rackup

### Running Background Workers

This one is a bit wonky, but it's pretty straightforward to get going:

Start Redis (if it's not going already)

    redis-server

Start sidekiq

    bundle exec sidekiq -r ./app/application.rb

## Man setup really blows!

Yeah, this is pretty shitty, let's improve this.  Most of these steps can probably just be
extracted into some shell scripts for the time being.  Until then, just keep updating this doc
with all the information your brains contain about the project so that if you become consumed by
zombies or zombusses we'll still be able to keep working on the thing.

Alternatively, we could place your brain in a jar and let you join the other lobotomites who have
come before ye.

## Contributors

* Christopher Saunders [web](http://christophersaunders.ca)
  [twitter](http://twitter.com/chris_saunders)
* David Underwood [web](http://theflyingdeveloper.com)
  [twitter](http://twitter.com/davefp)
* Ryan Coleman [twitter](http://twitter.com/coleman811)
