require 'sidekiq/capistrano'
require 'capistrano/ext/multistage'

set :stages, ['development', 'staging', 'production']
set :default_stage, 'development'

set :use_sudo, false
set :application, "meepleme"
set :user, "deploy"

set :scm, :git
set :repository, "git@bitbucket.org:csaunders/meepleme.git"
set :branch, "master"
set :git_shallow_clone, 1

set :deploy_via, :remote_cache

namespace :deploy do
  task :setup do
    setup_symlinks
    run "cd #{deploy_to}/current && bundle install"
  end

  task :start do
      run <<-CMD
      cd #{deploy_to}/current &&
      bundle exec compass compile &&
      nohup bundle exec thin -C config/production.yml -R config.ru start
    CMD
  end

  task :stop do
    run <<-CMD
      cd #{deploy_to}/current &&
      bundle install &&
      nohup bundle exec thin -C config/production.yml -R config.ru stop
    CMD
  end

  task :migrate do; end

  task :restart, :roles => [:web, :app] do
    setup_symlinks
    deploy.stop
    deploy.start
  end
end

def setup_symlinks
  run <<-CMD
  cd #{deploy_to}/current &&
  ln -sf /etc/apps/#{application}/production.yml #{deploy_to}/current/config/production.yml
  CMD
end