server "198.211.104.94", :app, :web, :db, :primary => true
set :deploy_to, "/home/#{user}/apps/#{application}"
set :sidekiq_cmd, "bundle install && bundle exec sidekiq -r ./app/application.rb"