require 'ostruct'
module Helpers
  module EventHelper
    SINGLE_DAY_FORMAT_STRING = "%b %-d, %Y" #May 1, 2013
    TIME_FORMAT_STRING = "%l:%M%P" # 12:30pm
    DAY_TIME_FORMAT_STRING = "#{SINGLE_DAY_FORMAT_STRING} at #{TIME_FORMAT_STRING}"
    def extract_event_time_information(event)
      time_info = {
        start_day: event.starts_at.strftime(SINGLE_DAY_FORMAT_STRING),
        start_time: event.starts_at.strftime(TIME_FORMAT_STRING),
        start_datetime: event.starts_at.strftime(DAY_TIME_FORMAT_STRING),
        end_day: event.ends_at.strftime(SINGLE_DAY_FORMAT_STRING),
        end_time: event.ends_at.strftime(TIME_FORMAT_STRING),
        end_datetime: event.ends_at.strftime(DAY_TIME_FORMAT_STRING)
      }
      OpenStruct.new(time_info)
    end

    def boardgame_image_classes(boardgame)
      classes = "boardgame-image"
      classes << " voted" if boardgame.user_voted?
      classes
    end
  end
end