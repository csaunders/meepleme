class BackgroundQueue
  def self.push(worker, params)
    worker.perform_async(params)
  end

  def self.processing?(job_id)
    queue = Sidekiq::Queue.new("default")
    queue.each do |job|
      return true if job.jid == job_id
    end
    false
  end
end