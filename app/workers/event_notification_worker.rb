class EventNotificationWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :notifications
  attr_accessor :service, :event

  def self.perform_async(params)
    event = Persistence::Event.get(params[:event_id])
    if expected_job_id = event.notification_job_id
      job_killer.kill_job {|job| job.jid == expected_job_id }
    end

    if job_id = enqueue(params)
      event.notification_job_id = job_id
      event.save
    end
  end

  def self.job_killer
    JobKiller.new('event')
  end

  def perform(params)
    @service = params[:service]
    @event = load_event(params[:event_id])
    notifier.notify
  end

  def load_event(id)
    Persistence::Event.get(id)
  end

  def notifier
    @notifier ||= NotificationService::Base.notifier(event, service)
  end

  private
  def self.enqueue(*args)
    # digging into the dirty details because mixins are a pain sometimes
    # this is effective calling a private function in Sidekiq
    client_push('class' => self, 'args' => args)
  end

end