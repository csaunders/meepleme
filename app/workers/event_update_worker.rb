class EventUpdateWorker
  include Sidekiq::Worker

  def perform(params={})
    processable_events.each do |event|
      event.update_from_remote
    end
  end

  def processble_events
    Persistence::Event.all(:starts_at.gt => Time.now.utc)
  end
end