class UserCollectionWorker
  include Sidekiq::Worker
  attr_accessor :user, :user_id, :boardgame_ids

  def perform(params)
    extract_params(params)
    self.user = find_user(user_id)
    user.boardgames = user.boardgames + get_games(boardgame_ids)
    user.save
  end

  def find_user(user_id)
    user_model.get(user_id)
  end

  def get_games(boardgame_ids)
    boardgame_model.all(:id => boardgame_ids)
  end

  def user_model
    Persistence::User
  end

  def boardgame_model
    Persistence::Boardgame
  end

  def extract_params(params)
    params = params.symbolize_keys
    self.user_id = params[:user_id]
    self.boardgame_ids = params[:boardgame_ids]
  end
end