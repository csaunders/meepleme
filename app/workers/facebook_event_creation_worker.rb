class FacebookEventCreationWorker
  include Sidekiq::Worker
  attr_accessor :event
  
  def perform(params)
    find_event(params['event_id'])
    event.event_service.invitations = params['invitations']
    event.event_service.construct!
  end

  def find_event(event_id)
    @event ||= Persistence::Event.get(event_id)
  end
end