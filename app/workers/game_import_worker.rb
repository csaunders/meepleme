class GameImportWorker
  include Sidekiq::Worker
  attr_accessor :api_client, :bgg_name, :associate_with_user

  def perform(params)
    extract_params(params)
    collection = fetch_collection
    collection = extract_collection(collection)
    save(collection)
    associate(collection) if associate_with_user
  end

  def fetch_collection(query_params={:own => 1})
    params = query_params.merge(:username => bgg_name)
    api_client.collection(params)
  end

  def extract_collection(collection)
    return unless collection['item'].size > 0
    games = collection['item']
    games.map do |entry|
      construct_boardgame(entry)
    end
  end

  def construct_boardgame(game)
    name = extract_name(game)
    cover_art_url = extract_image_url(game)
    bgg_id = extract_bgg_id(game)
    boardgame = boardgame_model.first_or_new(:bgg_id => bgg_id)
    boardgame.name = name
    boardgame.cover_art_url = cover_art_url
    boardgame
  end

  def extract_name(game)
    if game['name']
      entry = game['name'].first
      entry['content'] 
    end
  end

  def extract_image_url(game)
    game['image'].first if game['image']
  end

  def extract_bgg_id(game)
    game['objectid'].to_i if game['objectid']
  end

  def save(collection)
    collection.each(&:save) if collection && collection.size > 0
  end

  def associate(collection)
    boardgame_ids = collection.map{|b| b.id}
    user = user_model.first(:bggname => bgg_name)
    BackgroundQueue.push(associate_user_with_collection_worker, :user_id => user.id, :boardgame_ids => boardgame_ids)
  end

  def extract_params(params)
    params = params.symbolize_keys
    self.bgg_name = params[:bgg_name]
    self.api_client ||= params[:api_client_name].constantize.new
    self.associate_with_user = params[:associate_with_user] || false
  end

  def user_model
    Persistence::User
  end

  def boardgame_model
    Persistence::Boardgame
  end

  def associate_user_with_collection_worker
    UserCollectionWorker
  end
end