class Routable
  attr_accessor :route, :validator

  def initialize(args={})
    self.validator = args[:validator]
    self.route = args[:route]
  end

  def resolves?(item)
    validator.call(item)
  end

end