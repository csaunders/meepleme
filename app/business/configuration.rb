class Configuration

  def initialize
    environment = ENV['RACK_ENV']
    @config = YAML::load(File.read("config/#{environment}.yml")) rescue {}
  end

  def database_url
    @config['database_url']
  end

  def has_app?(name)
    !applications[name].nil?
  end

  def for_app(name)
    if has_app?(name)
      applications[name]
    end
  end

  private
  def applications
    @config['applications']
  end
end