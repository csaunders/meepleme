module Business
  class Logic
    attr_reader :persistable, :persistence_layer

    def initialize
      @persistable = persistence_layer.new
    end

    def persistence_layer
      @persistence_layer ||= begin
        mod = Module.const_get("Persistence")
        klass = self.class.to_s
        mod.const_get("#{klass}")
      end
    end

    def method_missing(meth, *args)
      if persistable.respond_to?(meth)
        persistable.send(meth, *args)
      elsif persistence_layer.respond_to?(meth)
        persistence_layer.send(meth, *args)
      else
        super
      end
    end
  end
end