class EventInformation
  extend Forwardable

  attr_reader :event, :user, :user_attendance
  attr_accessor :max_boardgames
  def_delegators :@event, :name, :single_day?, :location, :id, :description

  SINGLE_DAY_FORMAT_STRING = "%b %-d, %Y" #May 1, 2013
  TIME_FORMAT_STRING = "%l:%M%P" # 12:30pm
  DAY_TIME_FORMAT_STRING = "#{SINGLE_DAY_FORMAT_STRING} at #{TIME_FORMAT_STRING}"

  def initialize(options)
    @event = options[:event]
    @user = options[:user]
    @user_attendance = user.attendances.first(:event_id => event.id)
  end

  def attendees
    @attendees ||= event.attendances.all
  end

  def people_coming
    @people_coming ||= attendees.accepted
  end

  def user_attending?
    user_attendance.accepted?
  end

  def attending_link
    "/events/#{event.id}/attendance"
  end

  def boardgames
    @boardgames ||= organize_boardgames(categorize_boardgames)
  end

  def has_votes?
    top_boardgames.size > 0
  end

  def max_boardgames
    3
  end

  def top_boardgames
    @top_boardgames ||= boardgame_votes[0..(max_boardgames - 1)].map {|boardgame, votes| boardgame}
  end

  def boardgame_votes
    return @boardgame_votes if @boardgame_votes
    vote_data = event.votes.aggregate(:boardgame_id, :all.count)
    @boardgame_votes = vote_data.map do |boardgame_id, votes|
      [Persistence::Boardgame.get(boardgame_id), votes]
    end
  end

  def user_votes
    @user_votes ||= user_attendance.votes.all(:fields => [:boardgame_id]).map(&:boardgame_id)
  end

  def time_range
    "#{start_time} - #{end_time}" 
  end

  def start_day
    @start_day ||= event.starts_at.strftime(SINGLE_DAY_FORMAT_STRING)
  end

  def start_time
    @start_time ||= event.starts_at.strftime(TIME_FORMAT_STRING)
  end

  def start_datetime
    @start_datetime ||= event.starts_at.strftime(DAY_TIME_FORMAT_STRING)
  end

  def end_day
    @end_day ||= event.ends_at.strftime(SINGLE_DAY_FORMAT_STRING)
  end

  def end_time
    @end_time ||= event.ends_at.strftime(TIME_FORMAT_STRING)
  end

  def end_datetime
    @end_datetime ||= event.ends_at.strftime(DAY_TIME_FORMAT_STRING)
  end

  private

  def categorize_boardgames
    attendee_ids = attendees.map(&:user_id)
    boardgames = Persistence::Boardgame.all(:fields => [:id, :name, :cover_art_url], Persistence::Boardgame.users_boardgames.user_id => attendee_ids, :unique => true)
    boardgames.each do |boardgame|
      boardgame.user_voted = true if user_votes.include?(boardgame.id)
      boardgame
    end
  end

  def organize_boardgames(boardgames)
    result = []
    boardgames.each do |boardgame|
      boardgame.user_voted ? result.unshift(boardgame) : result.push(boardgame)
    end
    result
  end

end