module Socialite
  def graph
    @graph ||= Koala::Facebook::API.new(network_token(:facebook))
  end
end