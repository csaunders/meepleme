module EventService
  class Base

    attr_accessor :event

    def initialize(event)
      @event = event
    end

    def construct!
      raise NotImplementedError
    end

    def create_event
      raise NotImplementedError
    end

    def create_async(params={})
      job_params = default_params.merge(params)
      BackgroundQueue.push(worker, job_params)
    end

    def invite
      raise NotImplementedError
    end

    def event_link
      raise NotImplementedError
    end

    def worker
      NilWorker
    end

    def default_params
      {}
    end
  end
end