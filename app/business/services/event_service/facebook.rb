module EventService
  class Facebook < Base
    attr_accessor :invitations

    def create_event
      response = graph.put_connections("me", "events", event_params)
      response['id']
    end

    def get_event
      return nil unless event.service_id
      graph.get_object(event.service_id)
    end

    def get_attendee_ids
      return nil unless event.service_id
      attendees = graph.get_object("#{event.service_id}/attending")
      attendees.map do |attendee|
        attendee['id']
      end
    end

    def event_link
      "http://facebook.com/events/#{event.service_id}"
    end

    def invite
      graph.put_connections(event.service_id, 'invited', :users => invitations)
      invitations.each do |social_network_id|
        user = user_class.first(:social_network_id => social_network_id) || user_class.new(:social_network_id => social_network_id)
        attendance = event.attendances.new(:user => user)
        attendance.save
        user.save
      end
    rescue Koala::KoalaError => e
      puts e.message
    end

    def event_params
      {
        :name => event.name,
        :start_time => event.starts_at.to_s,
        :end_time => event.ends_at.to_s,
        :description => event.description,
        :privacy_type => event.privacy
      }
    end

    def construct!
      event.service_id = create_event
      invite if invitations.size > 0
      event.save
    end

    def default_params
      {
        :event_id => event.id,
        :invitations => invitations
      }
    end

    def worker
      FacebookEventCreationWorker
    end

    def invitations
      @invitations || []
    end

    def graph
      @graph ||= Koala::Facebook::API.new(event.creator.token)
    end

    def user_class
      Persistence::User
    end
  end
end