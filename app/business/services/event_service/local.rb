module EventService
  class Local < Base
    attr_accessor :invitations

    def create_event
      #Dummy endpoint, doesn't actually do anything with the event
      event.id
    end

    def event_link
      "/events/#{event.id}"
    end

    def invite
    end
  end
end