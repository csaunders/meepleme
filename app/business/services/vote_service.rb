class VoteService
  attr_accessor :event, :boardgame, :attendee
  attr_reader :vote

  def initialize(args)
    @event = args[:event]
    @boardgame = args[:boardgame]
    @attendee = args[:user].attendances.first(:event => event)
  end

  def vote
    return @vote if @vote
    attendee.votes.new(:boardgame => boardgame)
  end

  def vote!
    vote
    save
  end

  def save
    vote.save
  end
end