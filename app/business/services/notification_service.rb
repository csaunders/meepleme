%w(base facebook email nil_notifier).each do |service|
  require File.dirname(__FILE__) + "/notification_service/#{service}"
end
