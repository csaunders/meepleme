module NotificationService
  class Email < Base
    def notify
      raise StandardError.new("#notify needs to be implemented")
    end
  end
end