module NotificationService
  class Facebook < Base
    def notify
      graph.put_connections(event.service_id, "feed", notification_params)
    end

    def notification_params
      {message: notification_message}
    end

    def template
      @template ||= ERB.new(contents, 3, '-')
    end

    def contents
      contents =<<ERB
The games that we will be playing with are:

<% for game in @games -%>
  <%= game.name %> with <%= votes_message(game.votes) %>
<% end -%>
ERB
    end

    def votes_message(votes)
      "#{votes} " + if votes == 1
        "vote"
      else
        "votes"
      end
    end
  end
end
