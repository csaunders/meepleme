module NotificationService
  class Base
    include Socialite

    MAX_GAMES = 5

    attr_reader :event
    def initialize(event)
      @event = event
    end

    def network_token(network)
      case network
      when :facebook then event.creator.token
      else nil
      end
    end

    def notify
      raise NotImplementedError
    end

    def template
      raise NotImplementedError
    end

    def self.notifier(event, service=nil)
      if service.nil?
        warn "No service name was provided; using NilService instead" unless ENV['RACK_ENV'] == 'test'
        NotificationService::NilNotifier.new(event)
      else
        "NotificationService::#{service.camelize}".constantize.new(event)
      end
    end

    def notification_message
      @games = top_rated[0..[top_rated.size, MAX_GAMES].min]
      render
    end

    def render
      template.result(binding)
    end

    def top_rated
      @top_rated ||= repository(:default).adapter.select(top_rated_query, event.id)
    end

    private
    def top_rated_query
sql = <<SQL
SELECT
  boardgames.*, count(boardgames.id) as votes
FROM
  persistence_boardgames as boardgames,
  persistence_attendances as attendances,
  persistence_votes as votes
WHERE
  attendances.event_id = ? AND
  attendances.id = votes.attendance_id AND
  boardgames.id = votes.boardgame_id
GROUP BY boardgames.id
ORDER BY count(boardgames.id) DESC
SQL
    end
  end
end