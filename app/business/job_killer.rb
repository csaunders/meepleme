class JobKiller
  attr_reader :test, :queue_name

  def initialize(queue_name='default')
    @queue_name = queue_name
  end

  def kill_job(&blk)
    queue.each do |job|
      job.delete if yield(job)
    end
  end

  def queue
    @queue ||= Sidekiq::Queue.new(queue_name)
  end
end