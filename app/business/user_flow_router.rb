class UserFlowRouter
  class InvalidRoutingError < StandardError; end
  attr_accessor :routes, :user

  def initialize(user)
    @user = user
  end

  def route
    routes.each do |routable|
      return routable.route if routable.resolves?(user)
    end
    raise InvalidRoutingError.new("No valid route could be found for user-#{user.id}")
  end

  def routes
    @routes ||= default_routes
  end

  private

  def default_routes
    default_routes = []
    default_routes << Routable.new(:validator => lambda {|user| user.nil?}, :route => nil)
    default_routes << Routable.new(:validator => lambda {|user| user.bggname.blank?}, :route => "/")
    default_routes << Routable.new(:validator => lambda {|user| user.bggname.present?}, :route => "/events")
  end
end