require 'sidekiq/web'
def routes
  {
    "/" => AuthController.new,
    "/account" => AccountController.new,
    "/events" => EventsController.new,
    "/internal/queues" => Sidekiq::Web.new,
    "/admin" => AdminController.new
  }
end