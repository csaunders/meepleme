class AuthController < ApplicationController
  attr_reader :router
  before do
    @router = UserFlowRouter.new(current_user)
    session[:authenticating] = true
  end

  get '/' do
    if router.route && router.route != "/"
      redirect router.route
    else
      erb :"auth/index"
    end
  end

  get '/login' do
    redirect '/auth/facebook' unless current_user
  end

  get '/auth/:name/callback' do
    auth = request.env['omniauth.auth']

    user = Persistence::User.first(social_network_id: auth.uid) || Persistence::User.new(social_network_id: auth.uid)
    user.token = auth.credentials.token
    user.save

    session[:user_id] = user.id
    session.delete(:authenticating)
    redirect '/'
  end

  get '/logout' do
    session.clear
    redirect '/'
  end

  get '/about-us' do
    erb :"sales/about"
  end

  get '/privacy' do
    erb :"sales/privacy"
  end

  protected
  def requires_authentication?
    false
  end
end