class AccountController < ApplicationController

  get '/' do
    boardgames = current_user.boardgames.all(:order => [:name.asc])
    erb :"account/show", locals: {current_user: current_user, boardgames: boardgames}
  end

  post '/' do
    current_user.update(params['persistence-user'])
    flash[:notice] = 'Account updated!'
    redirect params[:redirect] ? params[:redirect] : to('/')
  end

end