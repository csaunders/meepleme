class EventsController < ApplicationController
  helpers Helpers::EventHelper
  attr_accessor :event

  before '/:id' do
    return unless params[:id].match(/\A\d+\Z/)
    @event = get_event
    unauthorized unless event.attendances.first(:user => current_user)
  end

  before '/' do
    events = if params[:hosting]
      current_user.owned_events
    else
      current_user.attendances.map do |attendance|
        attendance.event if attendance.involved?
      end
    end
    @events = events.compact.map{ |event| EventInformation.new(event: event, user: current_user) }
  end

  get '/' do
    erb :"event/index", locals: {events: @events}
  end

  get '/new' do
    facebook_conf = Configuration.new.for_app('facebook')
    @app_id = facebook_conf['app_id'] ? facebook_conf['app_id'] : ENV['APP_ID']
    @event = Persistence::Event.new
    erb :"event/new", locals: {app_id: @app_id, event: @event}
  end

  post '/create' do

    raw_params = params['persistence-event']
    event_params = {
      name: raw_params[:name],
      description: raw_params[:description],
      starts_at: extract_date(raw_params[:starts_at_date], raw_params[:starts_at_time]),
      ends_at: extract_date(raw_params[:ends_at_date], raw_params[:ends_at_time]),
      service_name: 'facebook',
      creator: current_user,
      invitations: raw_params[:invitations]
    }
    @event = Persistence::Event.new(event_params)
    if @event.save
      if @event.job_id
        erb :"event/create", locals: {event_id: @event.id, job_id: @event.job_id, platform: @event.service_name.capitalize}
      else
        redirect to("/#{@event.id}")
      end
    else
      erb :"event/new"
    end
  end

  get '/:id' do
    event_information = EventInformation.new(event: @event, user: current_user)
    erb :"event/show", locals: {event: event_information}
  end

  get '/create/status.json' do
    job_id = params[:job_id]
    content_type :json
    {:processing => BackgroundQueue.processing?(job_id)}.to_json
  end

  post '/:id/vote' do
    boardgame = Persistence::Boardgame.get(params[:boardgame_id])
    event = get_event
    vote_service = VoteService.new(:user => current_user, :boardgame => boardgame, :event => event)
    if vote_service.vote!
      flash[:notice] = "You voted for #{boardgame.name}"
    end
    redirect to("/#{event.id}")
  end

  post '/:id/attendance' do
    attendance = current_user.attendances.first(:event_id => params[:id])
    if params[:attending].downcase == 'yes'
      attendance.accept!
    else
      attendance.decline!
    end
    attendance.save
    redirect to("#{params[:id]}")
  end

  private

  def extract_date(date, time)
    DateTime.zone.parse("#{date} #{time}")
  rescue
    nil
  end

  def get_event
    Persistence::Event.get(params[:id])
  end

  def group_votes
    vote_data = @event.votes.aggregate(:boardgame_id, :all.count)
    vote_data.map do |boardgame_id, votes|
      [Persistence::Boardgame.get(boardgame_id), votes]
    end
  end

  def users_votes
    attendance = current_user.attendances.all(:event => @event).first
    attendance.votes.all(:fields => [:boardgame_id]).map(&:boardgame_id)
  end

  def group_boardgames
    attendance_ids = @event.attendances.all(:fields => [:user_id]).map(&:user_id)
    Persistence::Boardgame.all(:fields => [:id, :name, :cover_art_url], Persistence::Boardgame.users_boardgames.user_id => attendance_ids, :unique => true)
  end

end