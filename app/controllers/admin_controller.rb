class AdminController < ApplicationController
  before do
    redirect '/' unless current_user.admin?
  end

  get '/' do
    filter_and_render{ |user| true }
  end

  get '/users/creators' do
    filter_and_render do |user|
      user if user.creator?
    end
  end

  get '/users/attendees' do
    filter_and_render do |user|
      user if user.attendee?
    end
  end

  get '/users/inactive' do
    filter_and_render do |user|
      user if user.inactive?
    end
  end

  get '/users/:id' do
    return unless params[:id].match(/\A\d+\Z/)
    user = Persistence::User.get(params[:id])
    erb :"admin/show_user", locals: {user: user}
  end

  private

  def filter_and_render(&blk)
    users = Persistence::User.all.select{|u| blk.call(u)}
    erb :"admin/index", locals: {users: users}
  end

end