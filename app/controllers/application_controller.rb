class ApplicationController < Sinatra::Base
  helpers Sinatra::ContentFor
  register Padrino::Helpers
  SCOPE = %w(create_event user_events rsvp_event publish_stream).join(',')

  set :public_folder, File.dirname(__FILE__) + '/../assets'

  use Rack::Logger
  use Rack::Session::Cookie, :secret => "1f758f3c7c246388c55c3090f54fa28dd450e6e9cbe5eaa340d9d680d85c4038"
  use OmniAuth::Builder do
    configuration = Configuration.new
    fb_app_id = configuration.for_app('facebook')['app_id'] || ENV['APP_ID']
    fb_app_secret = configuration.for_app('facebook')['app_secret'] || ENV['APP_SECRET']
    provider :facebook, fb_app_id, fb_app_secret, :scope => SCOPE
  end

  use Rack::Flash, :accessorize => [:notice, :error]

  set :views, settings.root + '/../views'

  before do
    Time.zone = current_user.time_zone if current_user
    redirect '/' if request.path != '/' && current_user.nil? && requires_authentication?
  end

  get '/debugger' do
    puts ENV['RACK_ENV']
    if ENV['RACK_ENV'] == 'development'
      require 'debugger'
      debugger
    end
    'done'
  end

  protected
  def requires_authentication?
    true
  end

  def graph
    @graph ||= Koala::Facebook::API.new(current_user.token)
  end

  def current_user
    @current_user ||= Persistence::User.get(session[:user_id])
  end

  def logger
    request.logger
  end

  def unauthorized(message="You are not authorized to see that")
    halt 401, message
    nil
  end

end