module Persistence
  class UsersBoardgame
    include DataMapper::Resource

    property :id, Serial

    belongs_to :boardgame
    belongs_to :user
  end
end