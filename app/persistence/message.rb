module Persistence
  class Message
    include DataMapper::Resource

    property :id, Serial
    property :created_at, DateTime
    property :message, Text

    belongs_to :event
    belongs_to :user
  end
end