module Persistence
  class Vote
    include DataMapper::Resource

    property :id, Serial
    property :created_at, DateTime

    belongs_to :attendance
    belongs_to :boardgame

  end
end