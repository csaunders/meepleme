module Persistence
  class Boardgame
    include DataMapper::Resource
    attr_accessor :user_voted

    property :id, Serial
    property :bgg_id, Integer, :unique_index => :boardgame_geek_id, :min => 0, :max => 2**63 - 1 # unsigned bigint
    property :name, String
    property :playtime, Integer
    property :cover_art_url, URI

    has n, :users_boardgames

    def thumbnail
      cover_art_url.to_s.gsub(/\.(\w+)\Z/, '_t.\1')
    end

    def user_voted?
      @user_voted || false
    end
  end
end