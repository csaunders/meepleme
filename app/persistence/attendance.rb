module Persistence
  class Attendance
    include DataMapper::Resource

    property :id, Serial
    
    is :state_machine, :initial => :invited, :column => :status do
      state :invited
      state :declined
      state :accepted

      event :accept do
        transition :from => :invited, :to => :accepted
        transition :from => :declined, :to => :accepted
      end

      event :decline do
        transition :from => :invited, :to => :declined
        transition :from => :accepted, :to => :declined
      end
    end

    property :is_organizer, Boolean
    belongs_to :user
    belongs_to :event
    has n, :votes

    def accepted?
      status == 'accepted'
    end

    def declined?
      status == 'declined'
    end

    def involved?
      is_organizer || ['accepted', 'invited'].include?(status)
    end

    def self.invited
      all(:is_organizer => false, :status => 'invited')
    end

    def self.declined
      all(:is_organizer => false, :status => 'declined')
    end

    def self.accepted
      all(:is_organizer => false, :status => 'accepted')
    end
  end
end