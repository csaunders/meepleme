require 'data_mapper'
require 'dm-is-state_machine'
require 'yaml'
require 'dm-migrations/migration_runner'

unless ENV['SKIP_DB']

  case ENV['RACK_ENV'] || 'dev'
  when 'test'
    DataMapper::setup(:default, :adapter => :in_memory)
  when 'production'
    DataMapper::setup(:default, Configuration.new.database_url)
  else
    DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/dev.db")
  end

  require File.dirname(__FILE__) + '/user'
  require File.dirname(__FILE__) + '/message'
  require File.dirname(__FILE__) + '/event'
  require File.dirname(__FILE__) + '/users_boardgame'
  require File.dirname(__FILE__) + '/vote'
  require File.dirname(__FILE__) + '/boardgame'
  require File.dirname(__FILE__) + '/attendance'

  DataMapper.finalize
  if ENV['CLEAR']
    DataMapper.auto_migrate!
  else
    DataMapper.auto_upgrade!
  end
end
