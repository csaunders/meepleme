module Persistence
  class User
    attr_accessor :should_fetch
    include DataMapper::Resource

    property :id, Serial
    property :name, String
    property :token, Text
    property :social_network_id, Integer, :min => 0, :max => 2**63 - 1 # unsigned bigint
    property :bggname, Text
    property :date_format, String, :default => "%d %B, %Y"
    property :time_zone, String, :default => "(GMT-05:00) Eastern Time (US & Canada)"
    property :admin, Boolean, :default => false

    has n, :messages
    has n, :attendances
    has n, :users_boardgames
    has n, :boardgames, :through => :users_boardgames
    has n, :events, :through => :attendances
    has n, :owned_events, 'Event', :child_key => [ :creator_id ]

    before :save, :determine_if_collection_fetch_required
    after :save, :queue_fetch_collection

    def ghost?
      name.nil?
    end

    def admin?
      admin
    end

    def attendee?
      attendances.accepted.count > 0
    end

    def creator?
      owned_events.count > 0
    end

    def inactive?
      !(attendee? || creator?)
    end

    def determine_if_collection_fetch_required
      original_attributes.keys.each do |key|
        if key.name == :bggname
          self.should_fetch = true
          break
        end
      end
    end

    def queue_fetch_collection
      if should_fetch
        params = {
          :bgg_name => bggname,
          :api_client_name => BggApi.to_s,
          :associate_with_user => true
        }
        BackgroundQueue.push(GameImportWorker, params)
      end
    end
  end
end
