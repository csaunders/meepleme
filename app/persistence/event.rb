module Persistence
  class Event
    include DataMapper::Resource

    attr_accessor :service_name, :event_service
    attr_reader :job_id

    validates_with_method :validate_dates

    property :id, Serial
    property :service_id, Integer, :min => 0, :max => 2**63 - 1 # unsigned bigint
    property :service_name, String
    property :created_at, DateTime
    property :starts_at, DateTime, required: true
    property :ends_at, DateTime, required: true
    property :name, String, required: true
    property :description, Text
    property :notification_job_id, String
    property :location, String

    has n, :attendances
    has n, :messages
    has n, :votes, :through => :attendances
    has n, :users, :through => :attendances
    belongs_to :creator, 'User'

    delegate :invitations=, :invitations, :to => :event_service

    after :create do
      attendances.create(:user => creator, :is_organizer => true, :status => 'accepted')
      @job_id = event_service.create_async
    end

    def service_link
      event_service.event_link
    end

    def event_service
      @event_service ||= "EventService::#{service_name.camelize}".constantize.new(self)
    end

    def service_name
      super || 'local'
    end

    def service_name=(service)
      @event_service = nil
      super
    end

    def privacy
      'SECRET'
    end

    def single_day?
      format_string = "%Y-%m-%d"
      starts_at.strftime(format_string) == ends_at.strftime(format_string)
    end

    def update_from_remote
      event_information = event_service.get_event
      attendees = event_service.get_attendee_ids

      self.name = event_information['name']
      self.description = event_information['description']
      self.starts_at = DateTime.parse(event_information['start_time'])
      self.ends_at = DateTime.parse(event_information['end_time'])
      self.save

      known_attendee_ids = attendances.all(:id => attendees)
      attendees.each do |attendee_id|
        next if known_attendee_ids.include?(attendee_id)
        user = Persistence::User.new(:social_network_id => attendee_id)
        attendance = attendances.new(:user => user)
        attendance.save
        user.save
      end
    end

    private

    def validate_dates
      if !starts_at
        errors.add(:starts_at, "Event must have a start time")
        false
      elsif starts_at < Time.zone.now
        errors.add(:starts_at, "Event must start in the future")
        false
      elsif !ends_at
        errors.add(:ends_at, "Event must have an end time")
        false
      elsif ends_at < starts_at
        errors.add(:ends_at, "Event must end after it starts")
        false
      else
        true
      end
    end
  end
end