MeepleMe = {};
MeepleMe.Event = {
  Create: {
    checkIfDone: function(jobId, eventId) {
      $.ajax({
        url: '/events/create/status.json',
        data: {job_id: jobId},
        success: function(data, textStatus, jqXHR) {
          if(data.processing == false){
            window.location.replace("/events/" + eventId);
          }
        }
      });
    },
    initialize: function(jobId, eventId) {
      setInterval(function(){
        MeepleMe.Event.Create.checkIfDone(jobId, eventId);
      }, 3000);
    }
  }
};