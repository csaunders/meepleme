MeepleMe = {Event: {}}
MeepleMe.Event = {
  checkStatus: function(handler) {
    FB.getLoginStatus(function(response) {
      if(response.status === 'connected') {
        handler.success()
      } else {
        Event.Creator.login(handler);
      }
    });
  },

  login: function(handler) {
    FB.login(function(response) {
      if(response.authResponse) {
        handler.success()
      } else {
        handler.failure();
      }
    });
  },

  initialize: function(handler) {
    if(handler == undefined) {
      handler = {success: function(){}, failure: function(){}}
    }
    MeepleMe.Event.checkStatus(handler);
  },

  fetchFriends: function() {
    FB.api("/me/friends", MeepleMe.Event.friendsHandler)
  },

  friendsHandler: function(response) {
    friendsSelector = $('#persistence-event_invitations');
    $.each(response.data, function(index, value) {
      node = $('<option/>', {text: value.name, value: value.id});
      friendsSelector.append(node);
    });
    friendsSelector.chosen();
  }
};
