require 'clockwork'
require File.dirname(__FILE__) + '/application.rb'

module Clockwork
  handler do |job|
    puts "Running #{job}"
  end

  every(1.second, 'hourly.job') {
    EventUpdateWorker.perform_async
  }
end