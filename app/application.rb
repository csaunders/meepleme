require 'sinatra/base'
require 'sinatra/content_for'
require 'rack-flash'
require 'koala'
require 'omniauth-facebook'
require 'bgg-api'
require 'sidekiq'
require 'active_support/core_ext/numeric/time'
require 'active_support/core_ext/string/inflections'
require 'active_support/core_ext/hash/keys'
require 'active_support/core_ext/hash/slice'
require 'active_support/core_ext/module/delegation'
require 'logger'
require 'padrino-helpers'

if ENV['RACK_ENV'] == 'production'
  Dir.mkdir('logs') unless File.exist?('logs')
  $logger = Logger.new('logs/common.log', 'weekly')
  $logger.level = Logger::WARN

  $stdout.reopen("logs/output.log", "w")
  $stdout.sync = true
  $stderr.reopen($stdout)
else
  $logger = Logger.new(STDOUT)
end

require File.dirname(__FILE__) + '/helpers/helpers'
require File.dirname(__FILE__) + '/business/business'
require File.dirname(__FILE__) + '/persistence/persistence'
require File.dirname(__FILE__) + '/controllers/application_controller'
require File.dirname(__FILE__) + '/controllers/auth_controller'
require File.dirname(__FILE__) + '/controllers/account_controller'
require File.dirname(__FILE__) + '/controllers/events_controller'
require File.dirname(__FILE__) + '/controllers/admin_controller'
require File.dirname(__FILE__) + '/routes'
require File.dirname(__FILE__) + '/workers/workers'